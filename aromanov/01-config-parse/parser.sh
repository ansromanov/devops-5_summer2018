#!/usr/bin/env bash


function usage()
{
  printf "Usage:\\n"
  printf "%s -h" "$0"
  printf "%s -i=%s -o=%s\\n" "$0" "%input-file%" "%output-file%"
  printf "\\t-h --help \\t-"
  printf "\\twrites this help message\\n"
  printf "\\t-i --in-file \\t-"
  printf "\\tset input file. Mandatory parameter\\n"
  printf "\\t-o --out-file \\t-"
  printf "\\tset output file. If not set, will parse to STDOUT\\n"
}

while getopts "i:o:" opt; do
  case $opt in
    i)
      inputfile=$OPTARG 
      ;;
    o)
      outputfile=$OPTARG
      ;;
    *)
      usage
      ;;
  esac
done

if [ -f "$inputfile" ]; then
  printf "INPUT_FILE is \"%s\"\\n" "$inputfile"
  if [ -z "$outputfile" ]; then
    printf "ERROR: Output file not set\\n" >&2
    usage
    exit 2;
  fi
  touch "$outputfile" || exit 3
  printf "" > "$outputfile" || exit 4
  printf "OUTPUT_FILE is \"%s\"\\n" "$outputfile"
  while IFS= read -r line; do
    pattern=$(echo "$line" | grep -oP "{{ .*? }}" | tr -d "{} ")
    if [[ -n $pattern ]]; then
      for p in $pattern; do
        line=$(echo "$line" | sed -ne "s/{{ $p }}/${!p}/p")
      done
    fi
    echo "$line" >> "$outputfile"
  done  < "$inputfile"
else
  printf "ERROR: Input file \"%s\" not found\\n" "$inputfile" >&2
  usage
  exit 1;
fi
